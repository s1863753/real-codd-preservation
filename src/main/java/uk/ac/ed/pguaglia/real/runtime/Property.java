package uk.ac.ed.pguaglia.real.runtime;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class Property<T extends Enum<T>> {

	public EnumSet<T> options;
	public T currentOption;

	private final Class<T> enumType;
	private String name;
	private T defaultOption;

	public Property(String name, Class<T> enumType, T defaultOption) {
		this.name = name;
		this.options = EnumSet.allOf(enumType);
		this.enumType = enumType;
		this.defaultOption = defaultOption;
		this.currentOption = defaultOption;
	}

	public T getDefaultOption() {
		return defaultOption;
	}

	public void setDefaultOption(T defaultOption) {
		this.defaultOption = defaultOption;
	}

	public String usage(String prefix) {
		List<String> optList = new ArrayList<>();
		for (T opt : options) {
			optList.add(opt.toString().toLowerCase());
		}
		return String.format("%s.%s [ %s ]", prefix, name,
				String.join(" | ", optList));
	}

	public String status(String prefix) {
		return String.format("%s\"%s\"", prefix, currentOption.toString());
	}

	public boolean parseOption(String str) {
		try {
			currentOption = Enum.valueOf(enumType, str);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

	public List<String> optionsAsStrings() {
		List<String> opts = new ArrayList<>();
		for (T o : options) {
			opts.add(o.toString());
		}
		return opts;
	}
}
